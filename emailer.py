import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

def loadUserCredentials(secret_file):
    with open(secret_file, 'r') as sfile:
        file_contents = sfile.read()

        return file_contents.split('\n')

def emailer(to, subject, body, attachment=''):
    # Get the senders credentials
    sender = loadUserCredentials("./email_login.txt")

    # Set up the MIME
    message = MIMEMultipart()
    message['From'] = sender[0]
    message['To'] = to
    message['Subject'] = subject

    # Attach the mail body
    message.attach(MIMEText(body, 'plain'))

    # Attach the payload if any
    if len(attachment) > 0:
        payload = MIMEBase('application', 'octet-stream')
        
        with open(attachment) as attachment_file:
            payload.set_payload(attachment_file.read())

        encoders.encode_base64(payload)
        payload.add_header('Content-Decomposition', 'attachment', filename=attachment)
        message.attach(payload)

    # Convert the message to plain text
    text = message.as_string()

    # Handle authentication
    session = smtplib.SMTP('smtp.gmail.com', 587)
    session.starttls()
    session.login(sender[0], sender[1])

    # Send the message and close the connection
    session.sendmail(sender[0], to, text)
    session.quit()
    
